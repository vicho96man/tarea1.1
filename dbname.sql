PGDMP                      |            dbname    16.2    16.2 7    %           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            &           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            '           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            (           1262    16735    dbname    DATABASE     y   CREATE DATABASE dbname WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'Spanish_Spain.1252';
    DROP DATABASE dbname;
                postgres    false            �            1259    16744    actor    TABLE     �   CREATE TABLE public.actor (
    id_actor integer NOT NULL,
    nombre character varying(100) NOT NULL,
    edad integer NOT NULL
);
    DROP TABLE public.actor;
       public         heap    postgres    false            �            1259    16743    actor_id_actor_seq    SEQUENCE     �   CREATE SEQUENCE public.actor_id_actor_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.actor_id_actor_seq;
       public          postgres    false    218            )           0    0    actor_id_actor_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.actor_id_actor_seq OWNED BY public.actor.id_actor;
          public          postgres    false    217            �            1259    16762    actor_participa_en_pelicula    TABLE     u   CREATE TABLE public.actor_participa_en_pelicula (
    id_actor integer NOT NULL,
    id_pelicula integer NOT NULL
);
 /   DROP TABLE public.actor_participa_en_pelicula;
       public         heap    postgres    false            �            1259    16737    director    TABLE     �   CREATE TABLE public.director (
    id_director integer NOT NULL,
    nombre character varying(100) NOT NULL,
    edad integer NOT NULL
);
    DROP TABLE public.director;
       public         heap    postgres    false            �            1259    16736    director_id_director_seq    SEQUENCE     �   CREATE SEQUENCE public.director_id_director_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.director_id_director_seq;
       public          postgres    false    216            *           0    0    director_id_director_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.director_id_director_seq OWNED BY public.director.id_director;
          public          postgres    false    215            �            1259    16751    pelicula    TABLE       CREATE TABLE public.pelicula (
    id_pelicula integer NOT NULL,
    id_director integer NOT NULL,
    nombre character varying(200) NOT NULL,
    genero character varying(100) NOT NULL,
    duracion integer NOT NULL,
    fecha_publicacion date NOT NULL
);
    DROP TABLE public.pelicula;
       public         heap    postgres    false            �            1259    16750    pelicula_id_pelicula_seq    SEQUENCE     �   CREATE SEQUENCE public.pelicula_id_pelicula_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.pelicula_id_pelicula_seq;
       public          postgres    false    220            +           0    0    pelicula_id_pelicula_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.pelicula_id_pelicula_seq OWNED BY public.pelicula.id_pelicula;
          public          postgres    false    219            �            1259    16790    premio_mejor_actor    TABLE     �   CREATE TABLE public.premio_mejor_actor (
    id_premio integer NOT NULL,
    id_actor integer NOT NULL,
    anio integer NOT NULL
);
 &   DROP TABLE public.premio_mejor_actor;
       public         heap    postgres    false            �            1259    16789     premio_mejor_actor_id_premio_seq    SEQUENCE     �   CREATE SEQUENCE public.premio_mejor_actor_id_premio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.premio_mejor_actor_id_premio_seq;
       public          postgres    false    225            ,           0    0     premio_mejor_actor_id_premio_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.premio_mejor_actor_id_premio_seq OWNED BY public.premio_mejor_actor.id_premio;
          public          postgres    false    224            �            1259    16778    premio_mejor_director    TABLE     �   CREATE TABLE public.premio_mejor_director (
    id_premio integer NOT NULL,
    id_director integer NOT NULL,
    anio integer NOT NULL
);
 )   DROP TABLE public.premio_mejor_director;
       public         heap    postgres    false            �            1259    16777 #   premio_mejor_director_id_premio_seq    SEQUENCE     �   CREATE SEQUENCE public.premio_mejor_director_id_premio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.premio_mejor_director_id_premio_seq;
       public          postgres    false    223            -           0    0 #   premio_mejor_director_id_premio_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.premio_mejor_director_id_premio_seq OWNED BY public.premio_mejor_director.id_premio;
          public          postgres    false    222            �            1259    16802    premio_mejor_pelicula    TABLE     �   CREATE TABLE public.premio_mejor_pelicula (
    id_premio integer NOT NULL,
    id_pelicula integer NOT NULL,
    anio integer NOT NULL
);
 )   DROP TABLE public.premio_mejor_pelicula;
       public         heap    postgres    false            �            1259    16801 #   premio_mejor_pelicula_id_premio_seq    SEQUENCE     �   CREATE SEQUENCE public.premio_mejor_pelicula_id_premio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.premio_mejor_pelicula_id_premio_seq;
       public          postgres    false    227            .           0    0 #   premio_mejor_pelicula_id_premio_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.premio_mejor_pelicula_id_premio_seq OWNED BY public.premio_mejor_pelicula.id_premio;
          public          postgres    false    226            n           2604    16747    actor id_actor    DEFAULT     p   ALTER TABLE ONLY public.actor ALTER COLUMN id_actor SET DEFAULT nextval('public.actor_id_actor_seq'::regclass);
 =   ALTER TABLE public.actor ALTER COLUMN id_actor DROP DEFAULT;
       public          postgres    false    218    217    218            m           2604    16740    director id_director    DEFAULT     |   ALTER TABLE ONLY public.director ALTER COLUMN id_director SET DEFAULT nextval('public.director_id_director_seq'::regclass);
 C   ALTER TABLE public.director ALTER COLUMN id_director DROP DEFAULT;
       public          postgres    false    216    215    216            o           2604    16754    pelicula id_pelicula    DEFAULT     |   ALTER TABLE ONLY public.pelicula ALTER COLUMN id_pelicula SET DEFAULT nextval('public.pelicula_id_pelicula_seq'::regclass);
 C   ALTER TABLE public.pelicula ALTER COLUMN id_pelicula DROP DEFAULT;
       public          postgres    false    219    220    220            q           2604    16793    premio_mejor_actor id_premio    DEFAULT     �   ALTER TABLE ONLY public.premio_mejor_actor ALTER COLUMN id_premio SET DEFAULT nextval('public.premio_mejor_actor_id_premio_seq'::regclass);
 K   ALTER TABLE public.premio_mejor_actor ALTER COLUMN id_premio DROP DEFAULT;
       public          postgres    false    225    224    225            p           2604    16781    premio_mejor_director id_premio    DEFAULT     �   ALTER TABLE ONLY public.premio_mejor_director ALTER COLUMN id_premio SET DEFAULT nextval('public.premio_mejor_director_id_premio_seq'::regclass);
 N   ALTER TABLE public.premio_mejor_director ALTER COLUMN id_premio DROP DEFAULT;
       public          postgres    false    222    223    223            r           2604    16805    premio_mejor_pelicula id_premio    DEFAULT     �   ALTER TABLE ONLY public.premio_mejor_pelicula ALTER COLUMN id_premio SET DEFAULT nextval('public.premio_mejor_pelicula_id_premio_seq'::regclass);
 N   ALTER TABLE public.premio_mejor_pelicula ALTER COLUMN id_premio DROP DEFAULT;
       public          postgres    false    227    226    227                      0    16744    actor 
   TABLE DATA           7   COPY public.actor (id_actor, nombre, edad) FROM stdin;
    public          postgres    false    218   �C                 0    16762    actor_participa_en_pelicula 
   TABLE DATA           L   COPY public.actor_participa_en_pelicula (id_actor, id_pelicula) FROM stdin;
    public          postgres    false    221   sK                 0    16737    director 
   TABLE DATA           =   COPY public.director (id_director, nombre, edad) FROM stdin;
    public          postgres    false    216   �Q                 0    16751    pelicula 
   TABLE DATA           i   COPY public.pelicula (id_pelicula, id_director, nombre, genero, duracion, fecha_publicacion) FROM stdin;
    public          postgres    false    220   zY                  0    16790    premio_mejor_actor 
   TABLE DATA           G   COPY public.premio_mejor_actor (id_premio, id_actor, anio) FROM stdin;
    public          postgres    false    225   �o                 0    16778    premio_mejor_director 
   TABLE DATA           M   COPY public.premio_mejor_director (id_premio, id_director, anio) FROM stdin;
    public          postgres    false    223   �p       "          0    16802    premio_mejor_pelicula 
   TABLE DATA           M   COPY public.premio_mejor_pelicula (id_premio, id_pelicula, anio) FROM stdin;
    public          postgres    false    227   Nq       /           0    0    actor_id_actor_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.actor_id_actor_seq', 200, true);
          public          postgres    false    217            0           0    0    director_id_director_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.director_id_director_seq', 200, true);
          public          postgres    false    215            1           0    0    pelicula_id_pelicula_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.pelicula_id_pelicula_seq', 300, true);
          public          postgres    false    219            2           0    0     premio_mejor_actor_id_premio_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.premio_mejor_actor_id_premio_seq', 30, true);
          public          postgres    false    224            3           0    0 #   premio_mejor_director_id_premio_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.premio_mejor_director_id_premio_seq', 27, true);
          public          postgres    false    222            4           0    0 #   premio_mejor_pelicula_id_premio_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.premio_mejor_pelicula_id_premio_seq', 39, true);
          public          postgres    false    226            z           2606    16766 <   actor_participa_en_pelicula actor_participa_en_pelicula_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.actor_participa_en_pelicula
    ADD CONSTRAINT actor_participa_en_pelicula_pkey PRIMARY KEY (id_actor, id_pelicula);
 f   ALTER TABLE ONLY public.actor_participa_en_pelicula DROP CONSTRAINT actor_participa_en_pelicula_pkey;
       public            postgres    false    221    221            v           2606    16749    actor actor_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.actor
    ADD CONSTRAINT actor_pkey PRIMARY KEY (id_actor);
 :   ALTER TABLE ONLY public.actor DROP CONSTRAINT actor_pkey;
       public            postgres    false    218            t           2606    16742    director director_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.director
    ADD CONSTRAINT director_pkey PRIMARY KEY (id_director);
 @   ALTER TABLE ONLY public.director DROP CONSTRAINT director_pkey;
       public            postgres    false    216            x           2606    16756    pelicula pelicula_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.pelicula
    ADD CONSTRAINT pelicula_pkey PRIMARY KEY (id_pelicula);
 @   ALTER TABLE ONLY public.pelicula DROP CONSTRAINT pelicula_pkey;
       public            postgres    false    220            ~           2606    16795 *   premio_mejor_actor premio_mejor_actor_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.premio_mejor_actor
    ADD CONSTRAINT premio_mejor_actor_pkey PRIMARY KEY (id_premio);
 T   ALTER TABLE ONLY public.premio_mejor_actor DROP CONSTRAINT premio_mejor_actor_pkey;
       public            postgres    false    225            |           2606    16783 0   premio_mejor_director premio_mejor_director_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.premio_mejor_director
    ADD CONSTRAINT premio_mejor_director_pkey PRIMARY KEY (id_premio);
 Z   ALTER TABLE ONLY public.premio_mejor_director DROP CONSTRAINT premio_mejor_director_pkey;
       public            postgres    false    223            �           2606    16807 0   premio_mejor_pelicula premio_mejor_pelicula_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.premio_mejor_pelicula
    ADD CONSTRAINT premio_mejor_pelicula_pkey PRIMARY KEY (id_premio);
 Z   ALTER TABLE ONLY public.premio_mejor_pelicula DROP CONSTRAINT premio_mejor_pelicula_pkey;
       public            postgres    false    227            �           2606    16767 E   actor_participa_en_pelicula actor_participa_en_pelicula_id_actor_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.actor_participa_en_pelicula
    ADD CONSTRAINT actor_participa_en_pelicula_id_actor_fkey FOREIGN KEY (id_actor) REFERENCES public.actor(id_actor);
 o   ALTER TABLE ONLY public.actor_participa_en_pelicula DROP CONSTRAINT actor_participa_en_pelicula_id_actor_fkey;
       public          postgres    false    218    4726    221            �           2606    16772 H   actor_participa_en_pelicula actor_participa_en_pelicula_id_pelicula_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.actor_participa_en_pelicula
    ADD CONSTRAINT actor_participa_en_pelicula_id_pelicula_fkey FOREIGN KEY (id_pelicula) REFERENCES public.pelicula(id_pelicula);
 r   ALTER TABLE ONLY public.actor_participa_en_pelicula DROP CONSTRAINT actor_participa_en_pelicula_id_pelicula_fkey;
       public          postgres    false    220    221    4728            �           2606    16757 "   pelicula pelicula_id_director_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pelicula
    ADD CONSTRAINT pelicula_id_director_fkey FOREIGN KEY (id_director) REFERENCES public.director(id_director);
 L   ALTER TABLE ONLY public.pelicula DROP CONSTRAINT pelicula_id_director_fkey;
       public          postgres    false    216    4724    220            �           2606    16796 3   premio_mejor_actor premio_mejor_actor_id_actor_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.premio_mejor_actor
    ADD CONSTRAINT premio_mejor_actor_id_actor_fkey FOREIGN KEY (id_actor) REFERENCES public.actor(id_actor);
 ]   ALTER TABLE ONLY public.premio_mejor_actor DROP CONSTRAINT premio_mejor_actor_id_actor_fkey;
       public          postgres    false    4726    225    218            �           2606    16784 <   premio_mejor_director premio_mejor_director_id_director_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.premio_mejor_director
    ADD CONSTRAINT premio_mejor_director_id_director_fkey FOREIGN KEY (id_director) REFERENCES public.director(id_director);
 f   ALTER TABLE ONLY public.premio_mejor_director DROP CONSTRAINT premio_mejor_director_id_director_fkey;
       public          postgres    false    223    216    4724            �           2606    16808 <   premio_mejor_pelicula premio_mejor_pelicula_id_pelicula_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.premio_mejor_pelicula
    ADD CONSTRAINT premio_mejor_pelicula_id_pelicula_fkey FOREIGN KEY (id_pelicula) REFERENCES public.pelicula(id_pelicula);
 f   ALTER TABLE ONLY public.premio_mejor_pelicula DROP CONSTRAINT premio_mejor_pelicula_id_pelicula_fkey;
       public          postgres    false    4728    227    220               k  x�eWYv�8��V�V�ǲ-��@�	��?�{�Y��4�nz-����ɦ��ud�t�֕`�F��350�񔝴z�T��sP���g��2���p�q��,�yξ�N�jw�>����ݪV�����<�T��ݩ�⪒�Ojw���e�W�ܿ�Y��8,�fWj�ި��2ϊe�=xk��)"����9�����ab2�"c��Q��٫�<����g��ɾ�G���k=�օ��(�%e����X��jꌫY���/��"���V�_�;�ݏ��-y��wjV#�zjz���<�i�v��2Nx�K3��}�O3�ɽ:*D����y�RtE�Jvf�G��f���7�>�&1��-*��(ޤ����S=�۫X��}3 DV�W-U�V�G5QbY�n�{=���r��K%�D���E�Rv>��Hܖe�ʴj�:�Q���52��(x{t�:t�~-|Ⱦ�H������r�B
�V@�w8��?k�;��`R�<	xr?<�`��Sh	���!q}Z�<c_Tܽ�i
��6×j|Tm3Pҹ\!�M�,Ï\Wz�G{{�ܽ��{5��k� *v�[�c�W�uPV\&��ێ\
v= $W?�U�쳚֣G3v��vo<
��04���R����`�?_5�^Ӽ���l�U!��؅�݈�9 Ț�~/�����&NּH|�6��+^�����H]9C��Y ̫i֏�u�Ծ���0R�O�
_��T���^Z��W#�B;��,�!�5y`��هi��?k�ӵ	��`� G�J�������`-y�b�p��JZ����4!���)�B��L#�R�k��)(�W8�~3m���kʒ��Ά~�Z��\V�����J(����t(�A�X΢��,��gS"q�څ�tA��m;ީ��q�7�e�r�ԡ1�aEU��ا��+�]l��[�3�3n�U���-|`��4�|�-a#�u�.���tf��j�Q১����-Ӭ���jߑ
T��ê8?A89N���@, -Zx�@��A��@	>g���d���RW�f��v[�����0���$����Z�=.�O�2Yԉ$ݮgO���,B�.k��Hr?�d�5��d<���텡��^$��=�#��b�/��X�i��u� �_��2� "�X,q�8!��Ԙ��a[̃�j�,�q0b��_ie��;%B �@	x����(��"�q)a]��#������P��Q?l@�~@8�ׅe����$c�>��eTA�EJ#3�N7۶tZzZ�����LyA	8ɶ�`B $*c������
R�q��A���΂B��� �)�9+�~4	z�c�㞀A@�/��
��,�s:TD���ҹ��"����1�6hB�����$$��)�<A�� �MF�2�S`it�`]�b���9=+[*�7���}�O����I�RP������y��a�D�K8�� #���Pz��x"�!/H�p�Ȕy�S@4YPδ���Q��>""�;�[,���n�8XV�h�PT*���CB�(�"*�[0�3mu1��٨��aU� �i�И�,0�>9H�;��v�	EprX�V5V���s�6]X��<��$!�F�� x��j#l@|���|U�¡<�lv7�U�~�α���O���?�*'��ED�{*؟�~Ά�RE�����IG����?$�ig`���Zk�� ���~��ԩ���3c`{z@��A�5�]a�]�w���@���a�)h���Q�ݔ���
/��B�|	����@D�M=��d&	�����5u#@OO��9���� �b�,��C��Te"eh��2�hNgKGP��P�z}.��L讣��J�4I"�;��I~��s�/�D�|         _  x�=��u1C��br$�݋��#���?��WC�  rO�h��V֩c�Q{����uK�z��[����w��~n��^�W������ΜP(�
��<��{��ձ˽uy[yK_�Ӟ�ӿ���E ��W��l�>[���\%��ӯl�ytVQ��sͬ��ԹK�R{�"�J!yEj+�W�*��z�W�"?B�ruo�)"��k�1�S��tX���l<e�	yj�hE(=�� X���G*��� �uS�EO���ZUY%M�{��1�w��,�s��U%�Us/UHTJ_�)\��y��>�x��(���u�Beڝ������-n�n���<_���܂+oL�<�����~5�G]�&��x�Ǌ
'�� �+�FSO/$��Q�9��R�E/K9)��R�P�����u���x��Xc���׮�j�:Z�_=J|�������w�fˤ��JGx*�8�B�޻<��r��2�S:8��p�(	�����o�)��� ����F �m�I�/w�F��
��
��uU�7T�jXV�>e'^��#jBW�{�\����t U��+Űa�)-A:͒}��hk���ӈ�sQd�$��T��gev�rX`T
kMJ��0��
	�.�<^I5�������B�EwGJ	*����t��[a����a�����	[ ^�Ŧ�$�(�@�m��
IKl&�p���^ym��c㠀�+��C��f�s����L$���>F%�j�*�R�pD��08��(eC��l�%�t���� �j�Y<l9~��46�/i�ަsC��(J�$���%PpETa���	��Y���혥ąQ�z�m�凉��ǰ�n��a����2ݠ�����HxG�a2��)�j�m"����|�H���Ȩ
/�z� i�(�K��<�^���=`��M]�V@'����U#�s��/ ���$��{>�i��猓��dI�r�'uLA����ت��7���L��+Դ��hϭ#��uW�-̰S7�m���yUHe�q<=����u�i�4ܨ��0�i���}��>�8$�i*��)e��IЌ���;໺x탔:�tTj��؅�~���P�^m,��]uRÈF���S@�K9������o�x�`5)��&���3���]�)x ��=ǋ�5N�����1v��Dx8�)�V*��E�����x�7��P}�0��_�w�����ٽ<���9��N37�`cCVte!v��,&<L]�1o�����74�>��Y+�SOk΀(��V����F�C����g����2���7,�ur�������ј����I�o͵Ԗ���x-z�C��:WV�?ȗe��C�lS�3��3�ꖣ�c��"V(Dπn_�^H�Ѭ�
1�/��[a&�e	\R�� ��u۰�D�f'R��x����}Gd؈���y�� �x6k�#��Z;�!�c�&z2�P�FF�L)AFv��)F
��/����ϳs�v���c�9�N��F2�����u|�c�ޙ���o"˃α�`��l�o8�1G;�Nv2
��S���/�f1;E��ؔ \��㨫�-�n����1z���(y66��L	���g��x1��Y���,�ű�~d!�n-'��]��UUX��k?�>u�D����"]��?�j����,�         �  x�]WYv�8�N��#���7I�%ٲ,�z���"�Xd7��n3g��M$6V�Gz��dFFF��Cv��w����f��M/zЏ��������Q㑩�W�=�Ag���,y�.�_z;�4�a_z��ʒK�Q?���f��iofV7\��f�OVg_�G;h&s޲+��i�gw�E�F����4/L�\�IOG��쓙��4�.��z�_�ι(}l��8!�V�ם����)Q�ϫ;do�`_X-�h؃��^ӕ}7B�+��@B��q�ٛuYN��@�����U΋��ۣyξ��Ɵ��rO:{=�x��E��:d�{�̰:R�d:ݛ�����5`ʮ���.(x;,�B@�П���=,��b��f~BB�����*v�P��JO��6/Z���e�/�+=Mث/s�FO�DhK^
�A4^�8���%�R���;���e�
s;�&��_(�2�_���������aJ�nR<���M��DK�`�`x����������-{�w�E�88h�+�S���5�����Lٽ���!T�5��A���+��(^��InǧN����Xg�3�Uuz���Ȃv���h�Ķ�L[�)x��O���&��ԍ(3N���N��둲��[��[�Ș�@�.~���:)^��>��3�וC���<�X���d=�Q��qR�~������Lߡ�f��_T��H�D��-��`%z;/�HP�&ww�Y�?C�����?�{�NۦغJ+lqz�Ro�D��Pg4Uɛ��������M��˸�8�d,����VS�
;(��K���~?Rx2a���'2dk��=�־���U�eD�&k.�(�#~��R�����5�k_Y;���a�
�ѝnR�=��{b��	�Do�SA�ʹ�Q&Z.7�M[�"�R� kŕH|$��U�2mxCT	.?:=I���\U��m�j��tBݖ�&6��)��I?��<�ilT�g�۴A�զ�!4�͓�%�DiZ�b�݋�lv��i@:�&�Dڊ*�с1�@[�B�Q��o�����a\F7�d�:�6�*��U
[�$��X��S豃��"^��T�8ˋ�FPk��y�A���Q�Ş�)�,�2�24
���#�j[�L�\F�D�h7M�q��nL�y�Mo��>`��˝[�����}�@��j'F�y�[���ZU`�'J�j"$�I֛yF�ӘD��ф:��K�ɔ���&j��=���E�
�|��h��ďS��F՜7��?7þ��gL�w0�c��?@��g�-{gu�E 
2.�M��{+tS�*p�΋>��U.���m�
��9�Ia�nVm$�7�~����Z�|�x. T9�
*SY&LȖ$%������V�i.F�%�/+��+�8�����[I����v%ZJ��ԋ4��	x��N$�BT��L��	{"��6W�VTX�Ƭ�-��u�-��_5I�C%]+�M�ƗR�5Z�:������o)p� I'���r>���!��� &�F�	��*X�|B
&!���}�(a|���J��7�%��.x�'U��m�Fy"��X�zњ�?����$1� <ӤASlhgz9�	-��?)) ؆ሒ��4�C�U��ĺ	vԻXh��w�D!9I! 0�<��@*����@j ښ�l=�܎�~�I�m�b1�
����t��e��#�k"
lD ����W&�Z=�U��6Z�)��P�N$�H/;�n��v↾�~R	�%<�hND�/`%"0!*�7�������{b ,Q��&�y忁CV	�%�����-��v�6�D	� ,Ұ�ޟ.��pݓ℩8�S��v�r������:~��Crk�`^�M�+�پ3��},:^U�,�����OL����@��������+�:            x��\���8��ɯ��3$Pۇԧܝ�*��2���G-�����f�FX[m������dV����3H��@�J�C�e|:,����t��N���m:�Ǳ��t{��K������ύ}V�֕�l����a|O�i���/�|��2V�����>����M�=7�Y���������������t�'�oZ���0�"���]��ƪ��ჴ��8>�?~{�c�瑾x��}��l�ܸg�Ԇ~�<����o�m<��'������Yu_u�������.X7����߯�/��<�]*��Y�BU�J�l���r�N�`��t�>V�s����|�Ū�O2�yV]�*�`��8ߦ�T�&;�Ś��i���J�Y�v=�,;_����|�m����Sަ�2�_����=^��N7�������U�V�2�9��[�L��gd]|�j�+k���ja�|���?�M����6�]Mw��5N����O�1i	����h��yy�����ꭢ��-��o[��V�N�������[βcӲ�~�Z��}Y�Cn��2���[hZ�I���l{6���;�i8�'�\������-��o�xC7���rq�I�����
���;���FK������V�Od�_���|������j�n��uSә�����LK�����^������@P 4 ,pl��x:��b���mGD�M��0-�/�wrkˎ�tM�ht  ׯ��n��!?����O ��tp���^�S�1��i��v�L�� ���J���t�6�ߦ����>A
��km��Kw��=M����0�`���Դ�pi��5���麌G���������m�me���L��띢a,��+|�<Y�}���u&��Z8m8i�Pl$���L�{����Q�lX�o�p���1(���}T!��E�oU%��6]"^��-G�?�� �R�1��o���?��<Z6O�إl۪k71H˿]r���T�[c<z�#J>�]/>=]�/+P`g[����U_��{���t������2�܊��-Ë������q���/�q[�߳������<�4nq��n-9��|C��2QHo�C)�S����%���p1��2Et`&�2(����:��S��_(\@Y�}���5�g��{It	3ڱ&�B�w�nB�_O�i>��[�k�"�wl��<}�g_��N[�(;��vlLy��|yB�.��7�B4F � �v`Lj�nc����>�D� 06��E/�÷)�B�580n�V�p߈]��b��^���{Z��;�'	�����v��r�E ��?ʫ5أa/�O�wY��1�s�^l��s5����!_��� �<�p�`3�y�
��\�N���+� \��NF:��N������|/
�F�mZX�6z�x�N��_�S�Ã��L�צ��ƣ�����`�'��-��	�4좀���>�r��G9!"`PD��jhK K�TYе���m�4�%��ŭ1(��Q�C
gs�е�#e��]�<Gc�c�F~FZ8�$ޣ8�1�Q��M�B	gy�m�;�Ǚ��Dܕu?�6:�kc���1��S��{�ʩ}�$w���~E`����0�^N���m�ȡ�`L����ٵ���g�?��;�Uڪ� \��"�f�J(�#q����~�O�X	�:bob���RV�J�r��x�]�ï���a֒{Xø��̓u������2'��3�����Hv��z]��y��'�$핁��b�y*fZ$��j��=��7r$�������M��*Kk��ir��w��F0�5�}�
ްλef�x˴|BS�����!\��KY���}h���_o����$��)�z/1h�ǳ�$�K��z?���(��):��W{{p�5j�4r�^�p����ۈ�;�7�es��_V5�fh(����R���A�g�� P�UN��y_z��#�\���	u��ʔ k��X�4� �6��\q*�}ڰ6�oQ�5T$%NB��Z��(�O���ŰKc�a�_3���D�2�I����[�uŕN|��΂�HA�S��d��=�-�C�Kz� ��&Ҡ�s����ey#�R1Ԅ4nXE^��X�+f�=K{!��T5��� �|���� e�=��t���S��@��O��H��K�m0J&m��б:/Ľ\�a�ȓ#��.B%Z��`�F{5�@��U!�}�c����Y/ҭ�!�ȣV4~ϊ���l���>#l0��5����M)����u1�C�2	%X�D��#��1%H�Q�y�Z)�߮fiI�=�`��ZV�z�X��r���S侫^�N]9���2�)��G\c��q��a��o�l�³s����.Ԝc�v,��tش�<W��к��ż��X���PS#I,��0��t޶�0ނ����=uz�������ܨ�dE�*�gÞs�+^o�(��-!��M'��Ly%	���hN�P�Y�X�k��i��3 ��*Y���2��W�|�Ze�k�\����2�!�z:�RbE�����5���!�'��x9�U,:�b�	7��T$��)�����q�w?~O�xu�w�EI)�ѐ��=�N���R�*�뾵�j"M���(�u����8\�����Ǭ�A3/�|�ns��ȁ�Q�+���No�������O*V�L�ި-�,+����O��V�y_�zAإcm��I:����{,�sr�xE���ܕ�zTl������*z�0���S�q�i�/JE�Beh~�Ls2��I�)V۪�RQ��)�Ҡ䆚4;�u��95"*�K�nLѤ�ϫ�R�����>)��QU
��H1�(��nU��J|��my�ܘ�#;���G�\�m�~!�\��)�A�jܠi�^e�İN���E���]�H����)h�-�>:x��J����Ɨ�F���{N)I��%t Mp�֬��3WҦ�L����^�Xb�)�CÂ���Z 92y!��|AA��{���H��%�X'aM	�O�;4?�p�Q�$�-�z�bЉ�^�:�,�F�t�a���������DJ_)��\: (�zG��tOm����[�X�i�@n8���R����9�x�y��rR�Nm%�_���A����x��Q����?s���]r\0���N�*�CĠS"�g���E���d*��"�-�9�k'@�-^:~T����'�D�{��������i��y�ǿO�3@�s����4Q�!���{�1׆��7��x��vG��8^p&��6U��{5l@4ׂ#�`ȳ	����u��M��z9α�Lb�7b(y��\r�r��x�XOnYwRT
ZN5^G��6�b��W�]R�[��Z78_/���5�i�{����>�
���"��b*5;9��[�$AFh�|�>�2
qT���W�<�¬��W[0-w��� Ab?2��7�cUe��#�qu)�s����Rf�x�4Y�I#">߇C!�ń�x��4�e��{�S���\U{3��=�ѯT��%����ׂ�v۩z���|�Tŀgy�)v;#k߃qv��j-j�Ws��ம��p�ޔD��8� us܈rG���U�"��M�t{�#�w�th%2L�l=#,�lc��U�H͚B��Ey�q/@��Z
��>}O��1�:V2��n'Ϥ8=�"P��a*��]S2f:d$Y�P�(aUu�e�3�BYY��5��Xb؀D��Mi:p��O�"����X���!6f����ɚmS���[=��3����/!���focveTdʃ ��&��!JJ���K5!��N�C	�C�h��{����+ǌ�u��Pr��s�+tyvJA������Dd��5�%�������Pj?�f��	g��x�);Z�mў��	פ��1
����a��3L<��XUu�\D2����[�R]H9в7�HB�I�9���k����=��_@�Y���|>�*DŎ�����:�A�j���V�{�d�!����- gXW�����U�W���e©w�7�n��[
�8�'���6�k���i �2/Ի^f�Yb�KC�n��B+8�	�2����ݮ4� O  ���Q*�[�����'�H��������fy��`B��e�YG%�C�6���0�J�͇B�죩�2�����L��ekґ�n#�.�o[N���T4=��Ѧ��%Y���G_��B��f�C��C��+��;��15��R��8�7��T:'mZ�9�u�"�˄h��3X���ړ(�T���<��ynix��Zv
x�|ͦ�b'��M3�+����BrҪ�*�7䇓bN��g��ܟ�undV�gCy�ke3.���q*�g�W��}�B�7'慣�t,4�; �Ƙwg>=I��X�p��q��Y�$�}�N��p���ڏW� D'�,��'�
9EC�lՎ�b9S��8�j8]����F���V�M&�[<t�6r��b7�(����~�>��M���3`�Eh�t��!�U�c���p3Y�8�[�Q����X�NV�<���G��Ń��W	���m���'�]��Oau+�i�x�-��a�KA��%"z�A��[�=�3> ��O5���3��X�uUx����)Bv�KI�6�Ѱ���t�J(�g�� ���:�����+]���	����4��aw�*(Ri &�:���9�]�#�F��n��W���ɻΦ���s�3M���.��R3W.�W�%Π7���xÜ��q��9mָ��|�;�)V����d� xG>y`cz���C�Z(�oc���^�&�c��g/6
�
5̰*y:c��\7:r}��>��k2I�)?����YiG_�^�&CE�]+N��F_C$�&Ly�bU쪦Lh��a\}2+t&S5a��"�bn�Ů�]df|�B�A�a�~#�g�Z�B�M����D�Y�	���G�!��P1
.�ݎZT\�وMC�Xg3�Ѿ�a�أ����}�8V[N-��Y��Ki�v�G:��md�0��ʍƨ���M��YgeI��.����NQ��>+c8|Xk[EDr��HzY.{��I5̈́�D��x�Z�V�DsC1��%��--���e d]H%�،h�J\���Ϳ����P7^��}���de�Y��m���{[���1yu�������Å��R����֣� �����h^�*tP�S��v�*�gɫ|��t%}̆�M%�Rn�쵾0��z�v:�՛̉�)N�j����$�]?��ӢЃb�Y�N��̥����ڍ�ˢ
Y��.��R>���y�N��X����:�L�J����������zO��[��ΣE�"�[��E*�?���r�]&^$ �0��"5f�\���q��f�2}�R\�p_���<�d�����^��<��l��i��GÅ�
��N{�GHT����Q ����/Q�~�K@�=��D�J�OxϽ/S<���pVJ��a�iY���mx	�r���y���;�ڤ�`�S�;�e9Zt�y*�o�4Җ���~/ߣT�Ҽ"m���f�	�K���g���a�B����<��.go['��DJ�&���܇ �tk��{@��l�@xΔ�О���xZXC*1w�J��ʇ�2(��l՛m��A�^��{��o������r��$�	�=4ry�]d��Ѣki��F9�m}yuD�԰�q6���_����� ��2F          �   x�5��0B�f��8?{��?Gq������8�XgC�>��F�
L�)ɉ%9����8V��r@�^�:�tU�4��>`X��RR�mOp�����);Ttݕ��#L����1�p��Y�;�llmB��İ�q܁��nc��BF��̹�k�Z�v?��n�څ���5�         �   x�-��1�R1�1�^�w�i���.�ZM����܈=0��WX���L�xY�a�ٸ-��������N��h�ꚬM�>�&�q�Ӕ�VSµ�-�i�����+cM�6����KWf���;|pU/��Ӫ�cM)=��
�߭��`�/�      "   �   x�5Q���@:K1��:3j/���SA�9�S� ��iq.�[���<K�Ƶ\K�@ql�-����X��ւ��m�H��9�����W`���S4��z-�\�y����g&'��5v]�,�l˰.���LDX=�F���@l����Zd"�q��((˭�	٣�s,�:j�i�!����I�'��mRK����(�������55e[G[�6�d��J���? �JF�     